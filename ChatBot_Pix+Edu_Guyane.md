---
clavier: false
gestionGrosMots: true
avatar: https://dane.ins.ac-guyane.fr/sites/dane.ins.ac-guyane.fr/IMG/png/toucan.png
footer: false
---

# ChatBot Pix+Edu Guyane

> Bonjour, je suis un chatbot employé par la DRANE de Guyane.
Je suis en mesure de répondre à vos questions au sujet des parcours Pix+Edu

>Comment puis-je vous aider ?
> 
> 1. [Qu'est-ce que Pix+Edu ?](Rédaction réponse 1)
> 2. [Qu'est-ce que le parcours d'auto-positionnement ?](Rédaction réponse 2)
> 3. [Comment obtenir le code d'accès au parcours ?](Rédaction réponse 3)
> 4. [Comment puis-je me connecter au parcours ?](Rédaction réponse 4)
> 5. [Où vont les données ?](Rédaction réponse 5)
> 6. [Qui puis-je contacter ?](Rédaction réponse 6)
> 7. [Es-ce que je peux accéder à d'autres parcours Pix+Edu ?](Rédaction réponse 7)

## Rédaction réponse 1

Le dispositif Pix+ Édu vise à renforcer la culture numérique professionnelle des enseignants et des personnels d’éducation. Les compétences numériques professionnelles attendues sont définies dans le cadre de référence des compétences numériques pour l’éducation [(CRCN-Édu)](https://eduscol.education.fr/document/47366/download). Le dispositif est constitué d’un parcours d’auto-positionnement, de parcours d’auto-formation et d'entraînement en ligne, et de formations pour accompagner la montée en compétences.

Pour en savoir plus, je vous propose de consulter la [page Eduscol](https://eduscol.education.fr/3839/developpez-vos-competences-numeriques-avec-pix-edu) ou le [guide utilisateur Pix+édu](https://eduscol.education.fr/document/49598/download?attachment).

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)


## option 1
Que voulez-vous savoir ?
1. [Qu'est-ce que Pix+Edu ?](Rédaction réponse 1)
2. [Qu'est-ce que le parcours d'auto-positionnement ?](Rédaction réponse 2)
3. [Comment obtenir le code d'accès au parcours ?](Rédaction réponse 3)
4. [Comment puis-je me connecter au parcours ?](Rédaction réponse 4)
5. [Où vont les données ?](Rédaction réponse 5)
6. [Qui puis-je contacter ?](Rédaction réponse 6)
7. [Es-ce que je peux accéder à d'autres parcours Pix+Edu ?](Rédaction réponse 7)

## option 2
Très bien ! Je vous souhaite un bon parcours ! Je reste à votre disposition pour d'autres questions !


## Rédaction réponse 2

Le parcours d’auto-positionnement offre la possibilité, sur la base du volontariat et en moins de deux heures, de découvrir le dispositif Pix+édu, de tester ses compétences numériques pour l’éducation, et d’accéder à des tutoriels et ressources de formation en ligne.

Ce parcours couvre des compétences numériques transversales et professionnelles.

Il prend la forme de questions adaptées et de défis permettant de développer ses compétences.

<iframe src="https://podeduc.apps.education.fr/video/24170-developpez-vos-competences-numeriques-avec-pix-edu/?is_iframe=true" width="640" height="360" style="padding: 0; margin: 0; border:0" allowfullscreen title="Développez vos compétences numériques avec Pix+ Édu" ></iframe>

Pour en savoir plus, je vous propose de consulter la [page Eduscol](https://eduscol.education.fr/3839/developpez-vos-competences-numeriques-avec-pix-edu) ou le [guide utilisateur Pix+édu](https://eduscol.education.fr/document/49598/download?attachment).

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)

## Rédaction réponse 3

Pour vous donner accès à un parcours adapté, j'ai besoin d'en savoir un peu plus sur vous. Vous êtes : 

1. [Un professeur du 1er degré dans le public ?](option 3)
2. [Un professeur du 1er degré dans le privé ?](option 4)
3. [Un professeur du 2nd degré dans le public ?](option 5)
4. [Un professeur du 2nd degré dans le privé ?](option 6)
5. [Un Conseiller Principal d'éducation ?](option 7)
6. [Un Chef d'établissement ?](option 8)
7. [Un Inspecteur du 1er degré ?](option 9)
8. [Un Inspecteur du 2nd degré ?](option 10)

## option 3
Le code parcours pour les enseignants du 1er degré de l'enseignement public est le suivant : [ZTBHKM673](https://app.pix.fr/campagnes/ZTBHKM673)

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)

## option 4
Le code parcours pour les enseignants du 1er degré de l'enseignement privé est le suivant : [PCQDGS245](https://app.pix.fr/campagnes/PCQDGS245)

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)

## option 5
Le code parcours pour les enseignants du 2nd degré de l'enseignement public est le suivant : [NFXKWD117](https://app.pix.fr/campagnes/NFXKWD117)

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)

## option 6
Le code parcours pour les enseignants du 2nd degré de l'enseignement privé est le suivant : [TVAWHW214](https://app.pix.fr/campagnes/TVAWHW214)

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)

## option 7
Le code parcours pour les Conseiller Principaux d'Education de l'enseignement public est le suivant : [YSAEGV858](https://app.pix.fr/campagnes/YSAEGV858)

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)

## option 8
Le code parcours pour les personnels de direction est le suivant : [HDBQUT259](https://app.pix.fr/campagnes/HDBQUT259)

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)

## option 9
Le code parcours pour les Inspecteurs du 2nd degré est le suivant : [BXLRAW577](https://app.pix.fr/campagnes/BXLRAW577)

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)

## option 10
Le code parcours pour les Inspecteurs du 1er degré est le suivant : [AFMQYQ755](https://app.pix.fr/campagnes/AFMQYQ755)

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)

## Rédaction réponse 4

1. Créez un compte ou connectez-vous à votre compte Pix.fr
1. Accédez au message reçu dans votre messagerie académique, puis cliquez sur le lien de la campagne ou recopiez le code.
1. Renseignez votre adresse électronique professionnelle dans le champ à compléter avant de commencer le parcours.
1. Réalisez le parcours à votre rythme. Vous pouvez suspendre et reprendre le parcours à tout moment.
1. Envoyez vos résultats pour permettre aux équipes académiques d’ajuster l’accompagnement et les formations en fonction des besoins.

Je vous propose de consulter la [Fiche pratique sur l’accès au parcours d’auto-positionnement Pix+Edu](https://eduscol.education.fr/document/49601/download?attachment)

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)

## Rédaction réponse 5

Vos résultats sont enregistrés dans la plate-forme académique Pix et pourront être traités, de manière anonymisée, à des fins statistiques dans l’optique de la conception d’une offre de formation répondant au mieux aux besoins identifiés.

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)


## Rédaction réponse 6

Pour toute question concernant Pix et Pix+Edu vous pouvez vous adresser à Guillaume Allemann à l'adresse ambassadeur-pix@ac-guyane.fr

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)

## Rédaction réponse 7

Les compétences Pix+Edu liées au CRCN-Edu ne peuvent pas être travaillées depuis votre compte Pix.fr. Elle sont uniquement accessibles via des parcours d'entraînement spécifiques.Vous pouvez me demandez les codes de ces parcours en fonction de vos besoins : 

1. [Parcours d'entraînement pour le 1er degré](option 11)
2. [Parcours d'entraînement pour le 2nd degré](option 12)

## option 11
[Pix+ Édu 1er degré] Domaine 1 (CRCNÉdu): [UNAXYH929](https://app.pix.fr/campagnes/UNAXYH929)
[Pix+ Édu 1er degré] Domaine 2 (CRCNÉdu): [FZABRA381](https://app.pix.fr/campagnes/FZABRA381)
[Pix+ Édu 1er degré] Domaine 3 (CRCNÉdu): [DXXRYY531](https://app.pix.fr/campagnes/DXXRYY531)
[Pix+ Édu 1er degré] Domaines 4 & 5 (CRCNÉdu): [SVSLRW284](https://app.pix.fr/campagnes/SVSLRW284)
[Pix+ Édu 1er degré] Parcours Pix (CRCN): [ZCSJPZ442](https://app.pix.fr/campagnes/ZCSJPZ442)
[Pix+ Édu 1er degré] Thème Adapter les documents: [YCGSJC779](https://app.pix.fr/campagnes/YCGSJC779)
[Pix+ Édu 1er degré] Thème Communiquer collaborer: [MSAEVE556](https://app.pix.fr/campagnes/MSAEVE556)

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)

## option 12
[Pix+ Édu 2nd degré] Domaine 1 (CRCNÉdu): [QYMTHD335](https://app.pix.fr/campagnes/QYMTHD335)
[Pix+ Édu 2nd degré] Domaine 2 (CRCNÉdu): [QTMFMQ367](https://app.pix.fr/campagnes/QTMFMQ367)
[Pix+ Édu 2nd degré] Domaine 3 (CRCNÉdu): [HFPLTA677](https://app.pix.fr/campagnes/HFPLTA677)
[Pix+ Édu 2nd degré] Domaines 4 & 5 (CRCNÉdu): [BHAEDA142](https://app.pix.fr/campagnes/BHAEDA142)
[Pix+ Édu 2nd degré] Parcours Pix (CRCN): [LUSWBM489](https://app.pix.fr/campagnes/LUSWBM489)
[Pix+ Édu 2nd degré] Thème Communiquer collaborer: [HRFHPE859](https://app.pix.fr/campagnes/HRFHPE859)
[Pix+ Édu 2nd degré] Thème Adapter les documents: [ZNAVEJ529](https://app.pix.fr/campagnes/ZNAVEJ529)
[Pix+ Édu 2nd degré] Thème Créer des supports pédagogiques: [GTCXFW325](https://app.pix.fr/campagnes/GTCXFW325)
[Pix+ Édu 2nd degré] Thème Culture numérique: [WJZUQK734](https://app.pix.fr/campagnes/WJZUQK734)
[Pix+ Édu 2nd degré] Thème Gestion et partage de ressources: [ASCDPD895](https://app.pix.fr/campagnes/ASCDPD895)
[Pix+ Édu 2nd degré] Thème Numérique et sécurité: [KLARVV327](https://app.pix.fr/campagnes/KLARVV327)
[Pix+ Édu 2nd degré] Thème Réaliser une veille: [ZZFVWX523](https://app.pix.fr/campagnes/ZZFVWX523)

Avez-vous une autre question ?
1. [Oui ?](option 1)
2. [Non ?](option 2)